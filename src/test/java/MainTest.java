import com.google.gson.*;
import jdk.internal.util.xml.impl.Input;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Random;
import java.util.zip.GZIPInputStream;

public class MainTest {
    @Test
    public void testGetGoogle() throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet("http://google.com");
        CloseableHttpResponse response = httpClient.execute(httpGet);

        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("statusCode = " + statusCode);
        System.out.println("Description: " + response.getStatusLine().getReasonPhrase());

        for (Header header : response.getAllHeaders()) {
            System.out.println("name: " + header.getName() + ", " + header.getValue());
        }

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        String responseString = basicResponseHandler.handleResponse(response);

        System.out.println(responseString);
    }

    @Test
    public void testGetGoogleByIp() throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet("http://172.217.16.3");
        CloseableHttpResponse response = httpClient.execute(httpGet);

        httpGet.addHeader("Host", "google.pl");

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        String repsonseString = basicResponseHandler.handleResponse(response);

        System.out.println(repsonseString);
    }

    @Test
    public void testDeleteSda() throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpDelete httpDelete = new HttpDelete("https://sdacademy.pl/");
        //usuwanie zasobu na serwerze
        CloseableHttpResponse response = httpClient.execute(httpDelete);

        System.out.println("Status Code= " + response.getStatusLine().getStatusCode());

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        String responseString = basicResponseHandler.handleResponse(response);

        System.out.println(responseString);
    }

    @Test
    public void testSendDataToAcademy() throws IOException {
        //basic authentication jest na Base64 kodowaniu w obie strony
        HttpPost httpPost = new HttpPost("https://sdacademy.pl");
        httpPost.setEntity(EntityBuilder.create().setText("Hello there, General Kenobi").build());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        CloseableHttpResponse response = httpClient.execute(httpPost);

        System.out.println("Status code= " + response.getStatusLine().getStatusCode());

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        //handler bierze cala zawartosc strony jako string
        String responseString = basicResponseHandler.handleResponse(response);
    }

    @Test
    public void testJavaApi() throws IOException {//jak zle sformatujesz adres to rzuca malformed
////        czy mac tez ma c/programFiles
//        //java net biblioteka
//        URL url = new URL("https://sdacaemy.pl/");
//        URLConnection urlConnection = url.openConnection();
//        InputStream inputStream = urlConnection.getInputStream();
////        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//        //poniewaz http jest bardziej generalne nie ftp czy jakis inny
////        InputStream inputStream = httpURLConnection.getInputStream();
//        String line = null;
//        while ((line = new BufferedReader(new InputStreamReader(inputStream)).readLine()) != null){
//            System.out.println(line);
//        }
//        inputStream.close();


        URL url = new URL("https://sdacademy.pl/");
        URLConnection urlConnection = url.openConnection();

        InputStream inputStream = urlConnection.getInputStream();

//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new GZIPInputStream(inputStream)));
        String inputLine;
        while ((inputLine = bufferedReader.readLine()) != null) {
            System.out.println(inputLine);
        }

//        //------------------------wyswietla naglowki
//        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//        HttpGet httpGet = new HttpGet(String.valueOf(url));
//        CloseableHttpResponse response = httpClient.execute(httpGet);
//
//        for (Header header : response.getAllHeaders()) {
//            System.out.println("name: " + header.getName() + ", " + header.getValue());
//        }
//        //-------------------------------------------

        bufferedReader.close();
    }

    @Test
    public void testGetPolandInfo() throws IOException {
        String responseAsString = getJsonFromWebsite();
        System.out.println(responseAsString);
    }

    private String getJsonFromWebsite() throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet("https://restcountries.eu/rest/v2/name/poland");
        CloseableHttpResponse response = httpClient.execute(httpGet);

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        return basicResponseHandler.handleResponse(response);
    }

    @Test
    public void testGsonParse() {
        String json = "{\"firstname\":\"K\",\"lastname\":\"A\"}";
        JsonObject jsonObject = getJsonObject(json);
        JsonElement firstNameProperty = jsonObject.get("firstname");
        String firstName = firstNameProperty.getAsString();

        System.out.println("Imie :" + firstName);

    }

    private JsonObject getJsonObject(String json) {
        JsonParser parser = new JsonParser();
        JsonElement parsed = parser.parse(json);
        return parsed.getAsJsonArray().get(0).getAsJsonObject();
    }

    @Test
    public void testBuildSentence() throws IOException {
        String json = getJsonFromWebsite();
        JsonObject jsonObject = getJsonObject(json);

        StringBuilder sentence = new StringBuilder("Polska ma ");

        JsonElement population = jsonObject.get("population");
        sentence.append(population.getAsInt() + " ludnosci i sasiaduje z ");

        JsonArray borders = jsonObject.get("borders").getAsJsonArray();
        for (JsonElement border : borders) {
            sentence.append(border.toString().trim() + " ");
        }

        JsonElement translation = jsonObject.get("translations").getAsJsonObject();
        sentence.append(" a po japonsku to " + translation.getAsJsonObject().get("ja"));

        System.out.println(sentence);
    }

    @Test
    public void buildJsonFromObject() {
        System.out.println(buildRandomStudent());
    }

    private String buildRandomStudent() {
        Student student = new Student();
        Random random = new Random();
        student.setFirstName(String.valueOf((char) (60 + random.nextInt(25))));
        student.setLastName(String.valueOf((char) (60 + random.nextInt(25))));
        student.setAge(random.nextInt(40) + 20);

        Gson gson = new Gson();

        return gson.toJson(student);
    }

    @Test
    public void buildObjectFromJson() {
        String studentAsJson = buildRandomStudent();
        Gson gson = new Gson();
        Student student = gson.fromJson(studentAsJson, Student.class);
        System.out.println(student.getAge());
    }

    @Test
    public void sendStudentData() throws IOException {
        String studentAsJson = buildRandomStudent();
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost("https://protected-cove-60658.herokuapp.com/students");
        HttpEntity postData = EntityBuilder
                .create()
                .setText(studentAsJson)
                .setContentType(ContentType.APPLICATION_JSON)
                .build();
        httpPost.setEntity(postData);

        CloseableHttpResponse response = client.execute(httpPost);

        int statusCode = response.getStatusLine().getStatusCode();
        String reasonPhrase = response.getStatusLine().getReasonPhrase();

        System.out.println(statusCode);
        System.out.println(reasonPhrase);
    }
    @Test
    public void getStudentData() throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet ("https://protected-cove-60658.herokuapp.com/students");

        CloseableHttpResponse execute= httpClient.execute(httpGet);

        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        String response = basicResponseHandler.handleResponse(execute);

        System.out.println(response);

    }

}
